import { createAction, handleActions } from 'redux-actions';

// 액션 타입
const INCREMENT = 'counter/INCREMENT';
const DECREMENT = 'counter/DECREMENT';

// 리듀서
export default handleActions(
  {
    [INCREMENT]: (state, action) => state + 1,
    [DECREMENT]: (state, action) => state - 1,
  },
  0
);

// 액션 생성자
export const incrementCounter = createAction(INCREMENT);
export const decrementCounter = createAction(DECREMENT);
