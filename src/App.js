import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as counterActions from './modules/counter';

class App extends Component {
  render() {
    const { number, incrementCounter, decrementCounter } = this.props;

    return (
      <div>
        <h1>{number}</h1>
        <button onClick={incrementCounter}>+</button>
        <button onClick={decrementCounter}>-</button>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(counterActions, dispatch);

export default connect(
  state => ({
    number: state.counter,
  }),
  mapDispatchToProps
)(App);
